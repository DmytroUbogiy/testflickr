package flickr.test.com.flickrapp.base;

import javax.inject.Inject;
import flickr.test.com.flickrapp.mvp.view.BaseView;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dmytroubogiy on 02.12.17.
 */

public class BasePresenter<V extends BaseView> {
    @Inject
    protected V myView;

    public V getMyView() {
        return myView;
    }

    public <T> void subscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.newThread())
                .toSingle()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}
