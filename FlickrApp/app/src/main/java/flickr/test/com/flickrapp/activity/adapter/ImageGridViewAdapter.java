package flickr.test.com.flickrapp.activity.adapter;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flickr.test.com.flickrapp.R;
import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.presenter.LoadPhotoPresenter;

/**
 * Created by dmytroubogiy on 03.12.17.
 */

public class ImageGridViewAdapter extends RecyclerView.Adapter<ImageGridViewAdapter.Holder> {

    private List<Photo> items;
    private HashMap<String, Photo> loadedPhotos;
    private Set<String> sentIds;
    @Inject
    LoadPhotoPresenter loadPhotoPresenter;

    @Inject
    public ImageGridViewAdapter() {

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        Photo item = items.get(position);
        Photo loadedItem = loadedPhotos.get(item.getId());
        holder.loading.setTag(item);
        if (loadedItem == null) {
            if (!sentIds.contains(item.getId())) {
                holder.loading.setVisibility(View.VISIBLE);
                holder.image.setImageBitmap(null);
                loadPhotoPresenter.loadPhoto(item, position);
                sentIds.add(item.getId());
           }
        }
        else {
            Glide.with(holder.image.getContext())
                    .load(item.getImageUrl())
                    .into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public void setItems(List<Photo> items) {
        this.items = items;
        loadedPhotos = new HashMap<>();
        sentIds = new HashSet<>();
        notifyDataSetChanged();
    }

    public void updateItem(Photo photo, int position) {
        loadedPhotos.put(photo.getId(), photo);
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageViewPhoto)
        ImageView image;
        @BindView(R.id.progressBarLoading)
        ProgressBar loading;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            loading.setVisibility(View.GONE);
        }
    }
}
