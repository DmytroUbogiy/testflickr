package flickr.test.com.flickrapp.activity.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flickr.test.com.flickrapp.R;
import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.presenter.LoadPhotoPresenter;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

public class CustomSwipeAdapter extends PagerAdapter {

    private List<Photo> items;
    private HashMap<String, Photo> loadedPhotos;
    private Set<String> sentIds;
    private ViewHolder holder;
    @Inject
    LoadPhotoPresenter loadPhotoPresenter;
    LayoutInflater inflater;

    @Inject
    public CustomSwipeAdapter() {
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView view = new ImageView(container.getContext());
        if (container.getChildAt(position) == null) {
            container.addView(view);
        }
        Photo item = items.get(position);
        Glide.with(container.getContext())
                .load(item.getImageUrl())
                .into((ImageView) container.getChildAt(position));
        return view;
    }

    public void setItems(List<Photo> items) {
        this.items = items;
        loadedPhotos = new HashMap<>();
        sentIds = new HashSet<>();
        notifyDataSetChanged();
    }

    public void updateItem(Photo photo, int position) {
        items.add(photo);
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //container.removeViewAt(position);
    }

    class ViewHolder {

        @BindView(R.id.imageViewPhoto)
        ImageView imgPhoto;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
