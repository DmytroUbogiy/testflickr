package flickr.test.com.flickrapp.di.component;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import flickr.test.com.flickrapp.di.module.ApplicationModule;
import retrofit2.Retrofit;

/**
 * Created by dmytroubogiy on 01.12.17.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    Retrofit exposeRetrofit();
    Context exposeContext();
}
