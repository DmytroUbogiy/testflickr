package flickr.test.com.flickrapp.mvp.model;

import android.support.annotation.Nullable;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

public class PhotoResponse {
    public Photo photo;
    public Observable observable;
}
