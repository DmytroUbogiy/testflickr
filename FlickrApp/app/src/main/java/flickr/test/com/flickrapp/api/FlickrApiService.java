package flickr.test.com.flickrapp.api;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by dmytroubogiy on 02.12.17.
 */

public interface FlickrApiService {

    @GET
    Observable<Response<ResponseBody>> getImagesSet(@Url String url);
    @GET
    Observable<Response<ResponseBody>> getImageById(@Url String url);
}
