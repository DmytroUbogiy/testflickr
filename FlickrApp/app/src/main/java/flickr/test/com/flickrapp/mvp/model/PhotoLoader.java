package flickr.test.com.flickrapp.mvp.model;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;

import java.io.IOException;

import javax.inject.Inject;

import flickr.test.com.flickrapp.api.FlickrApiService;
import flickr.test.com.flickrapp.base.BasePresenter;
import flickr.test.com.flickrapp.mapper.PhotoMapper;
import flickr.test.com.flickrapp.mvp.view.MainView;
import flickr.test.com.flickrapp.mvp.view.PreviewView;
import flickr.test.com.flickrapp.util.FlickrUrls;
import flickr.test.com.flickrapp.util.Utils;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

public class PhotoLoader implements Observer<Response<ResponseBody>> {
    public Photo photo;
    public BasePresenter<PreviewView> view;
    public PhotoMapper photoMapper;
    public FlickrApiService apiService;

    public PhotoLoader(Photo photo, BasePresenter<PreviewView> view, FlickrApiService flickrApiService, PhotoMapper mapper) {
        this.view = view;
        this.photo = photo;
        this.apiService = flickrApiService;
        this.photoMapper = mapper;
    }

    public void loadPhoto() {
        String url = String.format(FlickrUrls.GET_PHOTO, Utils.API_KEY, photo.getId());
        Observable<Response<ResponseBody>> loadPhoto = apiService.getImageById(url);
        view.subscribe(loadPhoto, this);
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(Response<ResponseBody> responseBodyResponse) {
        if (responseBodyResponse != null) {
            try {
                String response = responseBodyResponse.body().string();
                view.getMyView().photoLoaded(photoMapper.getPhoto(response, photo), 0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
