package flickr.test.com.flickrapp.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import flickr.test.com.flickrapp.R;
import flickr.test.com.flickrapp.activity.MainActivity;
import flickr.test.com.flickrapp.base.BaseFragment;
import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.view.PreviewView;
import flickr.test.com.flickrapp.activity.adapter.CustomSwipeAdapter;
import flickr.test.com.flickrapp.util.TinderCart;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

public class ViewImageFragment extends BaseFragment implements PreviewView{

    @BindView(R.id.swipeView)
    SwipePlaceHolderView swipePlaceHolderView;
    @BindView(R.id.textViewDislike)
    TextView tvDislike;
    @BindView(R.id.textViewLike)
    TextView tvLike;

    CustomSwipeAdapter customSwipeAdapter;

    Unbinder unbinder;
    int selectedPage = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_pohoto, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipePlaceHolderView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f));
        presenter.loadPhotoSet();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }

    @Override
    public void photoLoaded(Photo photo, int position) {
        //presenter.updateItem(photo, position);
        super.onHide();
        TinderCart tinderCart = new TinderCart(getContext(), photo, swipePlaceHolderView);
        tinderCart.setPreviewView(this);
        swipePlaceHolderView.addView(tinderCart);
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        //viewPager.setAdapter(adapter);
    }

    @Override
    public void onLike() {
        StringBuilder like = new StringBuilder();
        like.append("LIKE: ");
        like.append(presenter.like());
        tvLike.setText(like);
    }

    @Override
    public void onDislike() {
        StringBuilder dislike = new StringBuilder();
        dislike.append("DISLIKE: ");
        dislike.append(presenter.dislike());
        tvDislike.setText(dislike);
    }
}
