package flickr.test.com.flickrapp.mvp.model;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by dmytroubogiy on 03.12.17.
 */

public class Photo {
    private String id;
    private String imageUrl;
    private Drawable drawable;
    private boolean isLike;
    private boolean isDislike;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isDislike() {
        return isDislike;
    }

    public void setDislike(boolean dislike) {
        isDislike = dislike;
    }
}
