package flickr.test.com.flickrapp.mvp.presenter;

import android.content.Context;

import java.io.IOException;

import javax.inject.Inject;

import flickr.test.com.flickrapp.api.FlickrApiService;
import flickr.test.com.flickrapp.base.BasePresenter;
import flickr.test.com.flickrapp.mapper.PhotoMapper;
import flickr.test.com.flickrapp.mvp.model.Photo;

import flickr.test.com.flickrapp.mvp.model.PhotoLoader;
import flickr.test.com.flickrapp.mvp.view.MainView;
import flickr.test.com.flickrapp.mvp.view.PreviewView;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observer;

/**
 * Created by dmytroubogiy on 03.12.17.
 */
public class LoadPhotoPresenter extends BasePresenter<PreviewView> {

    private Photo photo;
    @Inject
    FlickrApiService apiService;
    @Inject
    PhotoMapper photoMapper;
    private int position;

    @Inject
    public LoadPhotoPresenter() {
    }

    public void loadPhoto(Photo photo, int position) {
        new PhotoLoader(photo, this, apiService, photoMapper).loadPhoto();
    }

}
