package flickr.test.com.flickrapp.mvp.presenter;

import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import flickr.test.com.flickrapp.activity.adapter.CustomSwipeAdapter;
import flickr.test.com.flickrapp.activity.adapter.ImageGridViewAdapter;
import flickr.test.com.flickrapp.activity.fragment.ViewImageFragment;
import flickr.test.com.flickrapp.api.FlickrApiService;
import flickr.test.com.flickrapp.base.BasePresenter;
import flickr.test.com.flickrapp.mapper.PhotoSetMapper;
import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.model.PhotoLoader;
import flickr.test.com.flickrapp.mvp.model.PhotoSet;
import flickr.test.com.flickrapp.mvp.view.MainView;
import flickr.test.com.flickrapp.mvp.view.PreviewView;
import flickr.test.com.flickrapp.util.FlickrUrls;
import flickr.test.com.flickrapp.util.Utils;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;

/**
 * Created by dmytroubogiy on 02.12.17.
 */

public class PreviewPresenter extends BasePresenter<PreviewView> implements Observer<Response<ResponseBody>> {

    @Inject
    FlickrApiService flickrApiService;
    @Inject
    PhotoSetMapper photoSetMapper;
    @Inject
    CustomSwipeAdapter adapter;
    List<Photo> photos;
    @Inject
    LoadPhotoPresenter loadPhotoPresenter;
    int likeCount = 0;
    int dislikeCount = 0;

    @Inject
    public PreviewPresenter() {

    }

    public void loadPhotoSet() {
        getMyView().onShow("Loading...");
        String url = String.format(FlickrUrls.GET_PHOTOS_SET, Utils.API_KEY);
        Observable<Response<ResponseBody>> checkResponseObservable = flickrApiService.getImagesSet(url);;
        subscribe(checkResponseObservable, this);
    }

    public int like() {
        return ++likeCount;
    }

    public int dislike() {
        return ++dislikeCount;
    }

    public void loadUrls() {
        for (Photo photo: photos) {
            loadPhotoPresenter.loadPhoto(photo, photos.indexOf(photo));
        }
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    @Override
    public void onCompleted() {
        Log.d("Completed", "FINISH");
    }

    @Override
    public void onError(Throwable e) {
        getMyView().onError("Error");
        getMyView().onHide();
        Log.d("Error", e.getMessage());
    }

    @Override
    public void onNext(Response<ResponseBody> photoSetResponse) {
        if (photoSetResponse != null) {
            try {
                String response = photoSetResponse.body().string();
                PhotoSet photoSet = photoSetMapper.getPhotoSet(response);
                photos = photoSet.getPhoto();
                adapter.setItems(new ArrayList<Photo>());
                loadUrls();
                getMyView().setAdapter(adapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateItem(Photo photo, int position) {
        adapter.updateItem(photo, position);
    }
}
