package flickr.test.com.flickrapp.di.component;

import dagger.Component;
import flickr.test.com.flickrapp.activity.MainActivity;
import flickr.test.com.flickrapp.activity.fragment.PreviewFragment;
import flickr.test.com.flickrapp.activity.fragment.ViewImageFragment;
import flickr.test.com.flickrapp.base.BaseFragment;
import flickr.test.com.flickrapp.di.module.PreviewModule;
import flickr.test.com.flickrapp.di.scope.PerActivity;

/**
 * Created by dmytroubogiy on 02.12.17.
 */
@PerActivity
@Component(modules = {PreviewModule.class}, dependencies = {ApplicationComponent.class})
public interface PreviewComponent {
    void inject(PreviewFragment fragment);
    void inject(ViewImageFragment fragment);
    void inject(BaseFragment fragment);
}
