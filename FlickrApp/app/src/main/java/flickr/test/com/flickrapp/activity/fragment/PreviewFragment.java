package flickr.test.com.flickrapp.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import flickr.test.com.flickrapp.R;
import flickr.test.com.flickrapp.activity.MainActivity;
import flickr.test.com.flickrapp.base.BaseFragment;
import flickr.test.com.flickrapp.di.component.DaggerPreviewComponent;
import flickr.test.com.flickrapp.di.module.PreviewModule;
import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.presenter.PreviewPresenter;
import flickr.test.com.flickrapp.mvp.view.PreviewView;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

public class PreviewFragment extends BaseFragment implements PreviewView{

    @BindView(R.id.imagesList)
    RecyclerView lvPhotos;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeList();
        presenter.loadPhotoSet();
    }

    private void initializeList() {
        lvPhotos.setHasFixedSize(true);
        lvPhotos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void photoLoaded(Photo photo, int position) {
        presenter.updateItem(photo, position);
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {

        //lvPhotos.setAdapter(adapter);
    }

    @Override
    public void onLike() {
        super.onLike();
    }

    @Override
    public void onDislike() {
        super.onDislike();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unbinder.unbind();
    }
}
