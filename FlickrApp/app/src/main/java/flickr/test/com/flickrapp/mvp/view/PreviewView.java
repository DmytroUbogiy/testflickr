package flickr.test.com.flickrapp.mvp.view;

import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.BaseAdapter;

import flickr.test.com.flickrapp.mvp.model.Photo;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

public interface PreviewView extends BaseView{
    void photoLoaded(Photo photo, int position);
    void setAdapter(PagerAdapter adapter);
    void onLike();
    void onDislike();
    void onShow(String message);
    void onHide();
    void onError(String message);
}
