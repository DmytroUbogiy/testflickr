package flickr.test.com.flickrapp.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import flickr.test.com.flickrapp.application.FlickrApplication;
import flickr.test.com.flickrapp.di.component.ApplicationComponent;

/**
 * Created by dmytroubogiy on 02.12.17.
 */

public abstract class BaseActivity extends AppCompatActivity{
    private ProgressDialog pdDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        ButterKnife.bind(this);
        onViewReady(savedInstanceState, getIntent());
    }

    @CallSuper
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        //To be used by child activities
        resolveDaggerDependency();
    }

    protected void resolveDaggerDependency() {

    }

    public ApplicationComponent getApplicationComponent() {
        return ((FlickrApplication)getApplication()).getApplicationComponent();
    }

    public void showDialog(String message) {
        if (pdDialog == null) {
            pdDialog = new ProgressDialog(this);
            pdDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pdDialog.setCancelable(true);
        }
        pdDialog.setMessage(message);
        pdDialog.show();
    }

    public void hideDialog() {
        if (pdDialog != null && pdDialog.isShowing()) {
            pdDialog.dismiss();
        }
    }

    protected abstract int getContentView();
}
