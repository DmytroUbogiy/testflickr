package flickr.test.com.flickrapp.mvp.model;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by dmytroubogiy on 03.12.17.
 */

public class PhotoSet {
    private String id;
    private List<Photo> photo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }
}
