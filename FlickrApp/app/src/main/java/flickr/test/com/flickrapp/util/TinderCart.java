package flickr.test.com.flickrapp.util;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;

import flickr.test.com.flickrapp.R;
import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.view.PreviewView;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

@NonReusable
@Layout(R.layout.item_photo)
public class TinderCart {

    @View(R.id.imageViewPhoto)
    private ImageView imgPhoto;


    private Photo photo;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;
    private PreviewView previewView;

    public TinderCart(Context context, Photo photo, SwipePlaceHolderView swipeView) {
        mContext = context;
        this.photo = photo;
        mSwipeView = swipeView;
    }

    public void setPreviewView(PreviewView previewView) {
        this.previewView = previewView;
    }

    @Resolve
    private void onResolved(){
        Glide.with(mContext).load(photo.getImageUrl()).into(imgPhoto);
    }

    @SwipeOut
    private void onSwipedOut(){
        Log.d("EVENT", "onSwipedOut");
        previewView.onDislike();
        mSwipeView.addView(this);
    }

    @SwipeCancelState
    private void onSwipeCancelState(){
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn(){
        previewView.onLike();
        Log.d("EVENT", "onSwipedIn");
    }

    @SwipeInState
    private void onSwipeInState(){
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState(){
        Log.d("EVENT", "onSwipeOutState");
    }
}
