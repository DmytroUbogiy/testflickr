package flickr.test.com.flickrapp.util;

/**
 * Created by dmytroubogiy on 02.12.17.
 */

public class FlickrUrls {
    public final static String GET_PHOTOS_SET = "services/rest/?method=flickr.photos.getRecent&format=json&api_key=%s";
    public final static String GET_PHOTO = "services/rest/?method=flickr.photos.getSizes&format=json&api_key=%s&photo_id=%s";
}
