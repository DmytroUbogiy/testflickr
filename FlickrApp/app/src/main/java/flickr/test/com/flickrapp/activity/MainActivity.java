package flickr.test.com.flickrapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import flickr.test.com.flickrapp.R;
import flickr.test.com.flickrapp.activity.fragment.PreviewFragment;
import flickr.test.com.flickrapp.activity.fragment.ViewImageFragment;
import flickr.test.com.flickrapp.base.BaseActivity;
import flickr.test.com.flickrapp.di.module.PreviewModule;
import flickr.test.com.flickrapp.mvp.view.MainView;

public class MainActivity extends BaseActivity implements MainView {

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        openViewImage();
    }

    private void openPreview() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, new PreviewFragment());
        fragmentTransaction.commit();
    }

    private void openViewImage() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, new ViewImageFragment());
        fragmentTransaction.commit();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }


}
