package flickr.test.com.flickrapp.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import flickr.test.com.flickrapp.activity.MainActivity;
import flickr.test.com.flickrapp.di.component.DaggerPreviewComponent;
import flickr.test.com.flickrapp.di.module.PreviewModule;
import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.presenter.PreviewPresenter;
import flickr.test.com.flickrapp.mvp.view.PreviewView;

/**
 * Created by dmytroubogiy on 04.12.17.
 */

public class BaseFragment extends Fragment implements PreviewView {

    @Inject
    public
    PreviewPresenter presenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        resolveDaggerDependency();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected void resolveDaggerDependency() {
        DaggerPreviewComponent
                .builder()
                .applicationComponent(((MainActivity) getActivity()).getApplicationComponent())
                .previewModule(new PreviewModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void photoLoaded(Photo photo, int position) {

    }

    @Override
    public void setAdapter(PagerAdapter adapter) {

    }

    @Override
    public void onLike() {

    }

    @Override
    public void onDislike() {

    }

    @Override
    public void onShow(String message) {
        ((BaseActivity) getActivity()).showDialog(message);
    }

    @Override
    public void onHide() {
        ((BaseActivity) getActivity()).hideDialog();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
