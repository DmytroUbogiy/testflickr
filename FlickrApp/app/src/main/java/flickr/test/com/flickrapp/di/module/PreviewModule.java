package flickr.test.com.flickrapp.di.module;

import dagger.Module;
import dagger.Provides;
import flickr.test.com.flickrapp.api.FlickrApiService;
import flickr.test.com.flickrapp.di.scope.PerActivity;
import flickr.test.com.flickrapp.mvp.view.MainView;
import flickr.test.com.flickrapp.mvp.view.PreviewView;
import retrofit2.Retrofit;

/**
 * Created by dmytroubogiy on 02.12.17.
 */

@Module
public class PreviewModule {

    private PreviewView view;

    public PreviewModule(PreviewView view) {
        this.view = view;
    }

    @PerActivity
    @Provides
    PreviewView provideMainView() {
        return view;
    }

    @PerActivity
    @Provides
    FlickrApiService provideFlickrApiService(Retrofit retrofit) {
        return retrofit.create(FlickrApiService.class);
    }
}
