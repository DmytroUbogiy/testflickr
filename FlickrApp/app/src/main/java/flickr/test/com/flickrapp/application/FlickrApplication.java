package flickr.test.com.flickrapp.application;

import android.app.Application;

import flickr.test.com.flickrapp.di.component.ApplicationComponent;
import flickr.test.com.flickrapp.di.component.DaggerApplicationComponent;
import flickr.test.com.flickrapp.di.module.ApplicationModule;
import flickr.test.com.flickrapp.util.Utils;

/**
 * Created by dmytroubogiy on 01.12.17.
 */

public class FlickrApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeApplicationComponent();
    }

    private void initializeApplicationComponent() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this, Utils.BASE_URL))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
