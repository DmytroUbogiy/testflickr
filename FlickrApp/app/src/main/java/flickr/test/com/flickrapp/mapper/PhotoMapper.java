package flickr.test.com.flickrapp.mapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import flickr.test.com.flickrapp.mvp.model.Photo;

/**
 * Created by dmytroubogiy on 03.12.17.
 */

public class PhotoMapper {

    @Inject
    public PhotoMapper() {

    }

    public Photo getPhoto(String response, Photo photo) {
        String formatResponseFirst = response.replaceAll("jsonFlickrApi\\(", "");
        String formatResponse = formatResponseFirst.substring(0, formatResponseFirst.length() - 1);
        try {
            JSONObject jsonObject = new JSONObject(formatResponse);
            JSONObject sizes = jsonObject.getJSONObject("sizes");
            JSONArray imagesType = sizes.getJSONArray("size");
            for (int i = 0; i < imagesType.length(); ++i) {
                String label = imagesType.getJSONObject(i).getString("label");
                if (label.equals("Medium")) {
                    String url = imagesType.getJSONObject(i).getString("source");
                    photo.setImageUrl(url);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return photo;
    }
}
