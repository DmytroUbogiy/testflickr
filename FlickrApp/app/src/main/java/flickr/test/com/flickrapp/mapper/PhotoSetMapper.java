package flickr.test.com.flickrapp.mapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import flickr.test.com.flickrapp.mvp.model.Photo;
import flickr.test.com.flickrapp.mvp.model.PhotoSet;

/**
 * Created by dmytroubogiy on 03.12.17.
 */

public class PhotoSetMapper {

    @Inject
    public PhotoSetMapper() {

    }

    public PhotoSet getPhotoSet(String response) {
        PhotoSet photoSet = new PhotoSet();
        String formatResponseFirst = response.replaceAll("jsonFlickrApi\\(", "");
        String formatResponse = formatResponseFirst.substring(0, formatResponseFirst.length() - 1);
        try {
            JSONObject jsonObject = new JSONObject(formatResponse);
            JSONObject photoSetJson = jsonObject.getJSONObject("photos");
            photoSet.setPhoto(new ArrayList<Photo>());
            JSONArray photosArray = photoSetJson.getJSONArray("photo");
            for (int i = 0; i < photosArray.length(); ++i) {
                Photo photo = new Photo();
                String idPhoto = photosArray.getJSONObject(i).getString("id");
                photo.setId(idPhoto);
                photoSet.getPhoto().add(photo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return photoSet;
    }
}
